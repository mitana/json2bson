# json2bson – A simple tool to convert JSON to BSON

This is a **very** simple tool to convert JSON to BSON (even no --help).
It reads JSON from stdin and outputs BSON to stdout.

## Building

The tool is written in [D language](https://dlang.org). To build, please install
DMD. The **dub** package manager will be installed along with it.

Then run in the project directory:

```sh
dub build
```

## Usage

Provided that the executable is placed anywhere in the PATH, it is simple as
that:

``sh
json2bson
``

To convert a file, combine it with other tools, for example:

```sh
cat file.json | json2bson > file.bson
```
