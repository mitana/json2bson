import dutils.data.json;
import dutils.data.bson;

import std.algorithm.mutation;
import std.array;
import std.stdio;
import std.typecons;

BSON toBSON(JSON json) {
    return BSON.fromJSON(json);
}

void main()
{
	stdin.byLine(Yes.keepTerminator)
		.join()
		.idup
		.parseJSONString()
		.toBSON()
		.data()
		.copy(stdout.lockingBinaryWriter);
}
